import DetailPage from "@/components/template/DetailPage";
import { Container, Typography } from "@mui/material";

import { useRouter } from "next/router";

const MenuDetail = ({ data }) => {
  const router = useRouter();
  if (router.isFallback) {
    return <Typography>page is Loading ......</Typography>;
  }
  return (
    <Container sx={{ mt: 15 }}>
      <DetailPage {...data} />
    </Container>
  );
};

export default MenuDetail;

export async function getStaticPaths() {
  const res = await fetch(`${process.env.BASE_URL}/data`);
  const json = await res.json();

  console.log(json, "json");
  const data = json.slice(0, 10);
  const paths = data.map((food) => ({
    params: { menuId: food.id.toString() },
  }));

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps(context) {
  const {
    params: { menuId },
  } = context;
  const res = await fetch(`${process.env.BASE_URL}/data/${menuId}`);
  const data = await res.json();

  if (!data.name) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      data,
    },
    revalidate: 1 * 60 * 60,
  };
}
