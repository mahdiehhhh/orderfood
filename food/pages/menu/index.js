import MenuPage from "@/components/template/MenuPage";
import { Container } from "@mui/material";

const Menu = ({ data }) => {
  return (
    <Container sx={{ mt: 15 }}>
      <MenuPage data={data} />
    </Container>
  );
};

export default Menu;

export async function getStaticProps() {
  const res = await fetch(`${process.env.BASE_URL}/data`);
  const data = await res.json();
  return {
    props: {
      data,
      revalidate: 10,
    },
  };
}
