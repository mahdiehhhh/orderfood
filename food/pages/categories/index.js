import CategoriesPage from "@/components/template/CategoriesPage";
import { Container } from "@mui/material";

const Categories = ({ data }) => {
  console.log(data);
  return (
    <Container>
      <CategoriesPage data={data} />
    </Container>
  );
};

export default Categories;

export async function getServerSideProps(context) {
  const {
    query: { difficultly, time },
  } = context;
  const res = await fetch(`${process.env.BASE_URL}/data`);
  const data = await res.json();

  const filterResult = data.filter((item) => {
    const DifficultResult = item.details.filter(
      (detail) => detail.Difficulty && detail.Difficulty === difficultly
    );
    const TimResult = item.details.filter((detail) => {
      const CookingTime = detail["Cooking Time"] || "";
      const [TimeDetail] = CookingTime.split(" ");
      if (time === "less" && TimeDetail && +TimeDetail <= 30) {
        return detail;
      } else if (time === "more" && TimeDetail && +TimeDetail > 30) {
        return detail;
      }
    });
    if (time && difficultly && TimResult.length && DifficultResult.length) {
      return item;
    } else if (!time && difficultly && DifficultResult.length) {
      return item;
    } else if (time && !difficultly && TimResult.length) {
      return item;
    }
  });

  return {
    props: {
      data: filterResult,
    },
  };
}
