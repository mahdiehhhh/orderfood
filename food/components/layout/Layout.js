import { Box } from "@mui/material";
import Footer from "../module/Footer";
import Header from "../module/Header";

const Layout = ({ children }) => {
  return (
    <>
      <Header />
      <Box sx={{ minHeight: "100vh" }}>{children}</Box>
      <Footer />
    </>
  );
};

export default Layout;
