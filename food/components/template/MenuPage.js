import { Grid, Stack, Typography } from "@mui/material";
import CardMenu from "../module/CardMenu";
import { styled } from "@mui/material/styles";

const CustomTypography = styled(Typography)`
  /* Add your custom styles here */
  margin-bottom: 30px;
  border-bottom: 3px solid #53c60b;
  width: fit-content;
`;
const MenuPage = ({ data }) => {
  console.log(data);
  return (
    <Stack sx={{ mb: 10 }}>
      <CustomTypography
        variant='h3'
        component='h2'
        sx={{ ml: 12 }}>
        Menu
      </CustomTypography>
      <Grid
        sx={{ justifyContent: "center" }}
        container
        spacing={{ xs: 2, md: 3 }}
        columns={{ xs: 4, sm: 8, md: 18 }}>
        {data.map((food) => (
          <CardMenu
            key={food.id}
            {...food}
          />
        ))}
      </Grid>
    </Stack>
  );
};

export default MenuPage;
