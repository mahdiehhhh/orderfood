import {
  Box,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  Typography,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import CardMenu from "../module/CardMenu";
const CategoriesPage = ({ data }) => {
  const router = useRouter();

  const [query, setQuery] = useState({ difficultly: "", time: "" });

  const handleChange = (e) => {
    setQuery({ ...query, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    const RouterQuery = router.query;
    const { difficultly, time } = RouterQuery;
    if (query.difficultly !== difficultly || query.time !== time) {
      setQuery({ difficultly, time });
    }
  }, []);

  const searchHandler = () => {
    console.log(query);
    router.push({
      pathname: "/categories",
      query,
    });
  };

  const CustomTypography = styled(Typography)`
    /* Add your custom styles here */
    margin-bottom: 30px;
    border-bottom: 3px solid #53c60b;
    width: fit-content;
  `;
  return (
    <Stack sx={{ mt: 20 }}>
      <CustomTypography
        variant='h3'
        component='h2'>
        Menu
      </CustomTypography>
      <Grid
        sx={{ alignItems: "center" }}
        container
        spacing={{ xs: 2, md: 3 }}
        columns={{ xs: 4, sm: 8, md: 12 }}>
        <Grid
          item
          md={2}
          xs={2}>
          <FormControl
            sx={{ m: 1, minWidth: "150px" }}
            size='small'>
            <InputLabel id='demo-select-small-label'>difficultly</InputLabel>
            <Select
              labelId='demo-select-small-label'
              id='demo-select-small'
              value={query.difficultly}
              name='difficultly'
              label='difficultly'
              sx={{
                border: "none",
                width: "150px",
                height: "40px",
                borderRadius: " 10px",
                marginRight: "10px",
                padding: "10px",
                color: " #48ac0a",
                outline: "none",
                boxShadow:
                  "rgba(92, 245, 115, 0.123) 0px 4px 16px , rgba(17, 17, 26, 0.05) 0px 8px 32px",
              }}
              onChange={handleChange}>
              <MenuItem value=''>Difficultly</MenuItem>
              <MenuItem value='Hard'>Hard</MenuItem>
              <MenuItem value='Medium'>Medium</MenuItem>
              <MenuItem value='Easy'>Easy</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid
          item
          md={2}
          xs={2}>
          <FormControl
            sx={{ m: 1, minWidth: "150px" }}
            size='small'>
            <InputLabel id='demo-select-small-label'>CookingTime</InputLabel>
            <Select
              labelId='demo-select-small-label'
              id='demo-select-small'
              value={query.time}
              name='time'
              label='CookingTime'
              sx={{
                border: "none",
                width: "150px",
                height: "40px",
                borderRadius: " 10px",
                marginRight: "10px",
                padding: "10px",
                color: " #48ac0a",
                outline: "none",
                boxShadow:
                  "rgba(92, 245, 115, 0.123) 0px 4px 16px , rgba(17, 17, 26, 0.05) 0px 8px 32px",
              }}
              onChange={handleChange}>
              <MenuItem value=''>CookingTime </MenuItem>
              <MenuItem value='more'>More than 30 min </MenuItem>
              <MenuItem value='less'>Less than 30 min </MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid
          item
          md={2}
          xs={4}>
          <button
            style={{
              background: "#53c60b",
              color: "#ffff",
              outline: "none",
              width: "100px",
              border: "none",
              borderRadius: "7px",
              height: "40px",
              cursor: "pointer",
            }}
            onClick={searchHandler}>
            Search
          </button>
        </Grid>
      </Grid>

      <Stack sx={{ mt: 5 }}>
        {data.length ? (
          <Grid
            sx={{ justifyContent: "center" }}
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 4, sm: 8, md: 18 }}>
            {data.map((food) => (
              <CardMenu
                key={food.id}
                {...food}
              />
            ))}
          </Grid>
        ) : (
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <img
              src='/image/search.png'
              alt='search'
              width='400px'
            />
          </Box>
        )}
      </Stack>
    </Stack>
  );
};

export default CategoriesPage;
