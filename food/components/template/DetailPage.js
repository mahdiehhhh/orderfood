import {
  Box,
  Container,
  Grid,
  List,
  ListItem,
  Stack,
  Typography,
  colors,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import LocalAtmIcon from "@mui/icons-material/LocalAtm";
import LocationOnIcon from "@mui/icons-material/LocationOn";
const CustomTypography = styled(Typography)`
  /* Add your custom styles here */
  margin-bottom: 30px;
  border-bottom: 3px solid #53c60b;
  width: fit-content;
`;
const DetailPage = (props) => {
  const {
    id,
    name,
    price,
    discount,
    introduction,
    details,
    ingredients,
    recipe,
  } = props;
  return (
    <Container>
      <CustomTypography
        variant='h3'
        component='h2'>
        Dtails
      </CustomTypography>
      <Grid
        sx={{ justifyContent: "center" }}
        container
        spacing={{ xs: 2, md: 3 }}
        columns={{ xs: 4, sm: 8, md: 12 }}>
        <Grid
          item
          md={6}>
          <img
            src={`/image/${id}.jpeg`}
            width='100%'
            style={{ borderRadius: "7px" }}
          />
        </Grid>
        <Grid
          item
          md={6}>
          <Typography
            variant='h4'
            color='#53c60b'>
            {name}
          </Typography>
          <Stack
            direction='row'
            alignItems='center'
            sx={{ mt: 3 }}>
            <LocationOnIcon sx={{ paddingRight: "5px", fontSize: "25px" }} />
            <Typography>{details[0].Cuisine}</Typography>
          </Stack>

          <Stack
            direction='row'
            alignItems='center'
            sx={{ mt: 3 }}>
            <LocalAtmIcon sx={{ paddingRight: "10px", fontSize: "30px" }} />
            <Typography
              variant='body2'
              sx={{ color: discount ? "red" : "black" }}>
              {discount ? (price * (100 - discount)) / 100 : price} $
            </Typography>
          </Stack>
          {discount ? (
            <Typography
              variant='body2'
              sx={{
                backgroundColor: discount ? "red" : "",
                mt: 4,
                width: "150px",
                padding: "5px",
                borderRadius: "7px",
                color: "white",
                textAlign: "center",
              }}>
              {discount}$ OFF
            </Typography>
          ) : null}
        </Grid>
      </Grid>

      <Typography
        variant='body2'
        sx={{ mt: 4, lineHeight: "30px" }}>
        {introduction}
      </Typography>

      <Typography
        variant='h4'
        color='#53c60b'
        sx={{ mt: 4 }}>
        Details
      </Typography>
      <List sx={{ listStyleType: "disc", pl: 4 }}>
        {details.map((detail, index) => (
          <ListItem
            key={index}
            sx={{ display: "list-item", pl: 0 }}>
            {Object.keys(detail)[0]}:{Object.values(detail)[0]}
          </ListItem>
        ))}
      </List>

      <Typography
        variant='h4'
        color='#53c60b'
        sx={{ mt: 4 }}>
        Ingredients
      </Typography>
      <List sx={{ listStyleType: "disc", pl: 4 }}>
        {ingredients.map((ingredient, index) => (
          <ListItem
            key={index}
            sx={{ display: "list-item", pl: 0 }}>
            {ingredient}
          </ListItem>
        ))}
      </List>
      <Typography
        variant='h4'
        color='#53c60b'
        sx={{ my: 4 }}>
        recipe
      </Typography>

      <Box sx={{ mb: 4 }}>
        {recipe.map((item, index) => (
          <Box
            key={index}
            sx={{
              backgroundColor: index % 2 ? "#e8ffdb" : "#bcff93",
            }}>
            <Typography
              variant='body2'
              sx={{ padding: "15px" }}>
              {index + 1} {item}
            </Typography>
          </Box>
        ))}
      </Box>
    </Container>
  );
};

export default DetailPage;
