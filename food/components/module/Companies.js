import { Container, Grid } from "@mui/material";
import Apple from "../../icons/Apple";
import Binance from "../../icons/Binance.js";
import SpaceX from "../../icons/SpaceX.js";
import Tesla from "../../icons/Tesla.js";
const items = [
  { id: 1, icon: <Apple /> },
  { id: 2, icon: <Binance /> },
  { id: 3, icon: <SpaceX /> },
  { id: 4, icon: <Tesla /> },
];

const Companies = () => {
  return (
    <Container sx={{ marginTop: "4rem" }}>
      <Grid
        container
        sx={{ display: "flex", alignItems: "center" }}
        spacing={{ xs: 2, md: 3 }}
        columns={{ xs: 4, sm: 8, md: 12 }}>
        {items.map((item) => (
          <Grid
            sx={{ display: "flex", justifyContent: "center" }}
            key={item.id}
            md={3}
            xs={2}>
            {item.icon}
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default Companies;
