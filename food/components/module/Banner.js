import { Container, Grid, Stack, Typography } from "@mui/material";
import { styled } from "@mui/material/styles";
import Link from "next/link";

const CustomTypography = styled(Typography)`
  /* Add your custom styles here */
  margin-bottom: 30px;
  border-bottom: 3px solid #53c60b;
  width: fit-content;
`;

const Banner = () => {
  return (
    <Container sx={{ marginTop: "8rem" }}>
      <Grid
        sx={{ display: "flex", alignItems: "center" }}
        container
        spacing={{ xs: 2, md: 3 }}
        columns={{ xs: 4, sm: 8, md: 12 }}>
        <Grid
          item
          md={6}>
          <CustomTypography
            variant='h3'
            component='h2'>
            2SISIFood
          </CustomTypography>

          <Typography
            variant='body1'
            sx={{ fontWeight: "bold", marginBottom: "10px" }}>
            Food Delivery and Takeout!
          </Typography>
          <Typography
            variant='body2'
            sx={{
              marginBottom: "20px",
              lineHeight: "25px",
              maxWidth: "400px",
            }}>
            BotoFood is an online food ordering and delivery platform launched
            by Uber in 2014. Meals are delivered by couriers using cars,
            scooters, bikes, or on foot.
          </Typography>

          <Link
            href='/menu'
            style={{
              backgroundColor: "#53c60b",
              color: "white",
              padding: "5px 20px",
              borderRadius: "5px",
            }}>
            See All
          </Link>
        </Grid>
        <Grid
          item
          md={6}>
          <Stack sx={{ display: "flex", justifyContent: "center" }}>
            <img
              src='/image/banner.png'
              alt='banner'
              width='80%'
              style={{ display: "table", margin: "auto" }}
            />
          </Stack>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Banner;
