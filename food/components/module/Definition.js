import { Box, Container, Typography } from "@mui/material";

const Definition = () => {
  return (
    <Container sx={{ marginTop: "8rem" }}>
      <Typography
        variant='h4'
        component='h4'
        color='#53c60b'>
        Who We Are?
      </Typography>

      <Box mt={5}>
        <Typography
          variant='body2'
          sx={{ lineHeight: "25px" }}>
          BotoFood company was founded in 2009 by Garrett Camp and Travis
          Kalanick.The company began food delivery in August 2014 with the
          launch of the UberFRESH service in Santa Monica, California. In 2015,
          the platform was renamed to UberEATS and the ordering software was
          released as its own application, separate from the app for Uber
          rides.In 2016, it commenced operations in both London and Paris.
        </Typography>
      </Box>
    </Container>
  );
};

export default Definition;
