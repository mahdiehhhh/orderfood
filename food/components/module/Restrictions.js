import { Container, Typography } from "@mui/material";

const Restrictions = () => {
  return (
    <Container sx={{ marginY: "5rem" }}>
      <Typography
        variant='h4'
        component='h4'
        color='#53c60b'>
        Restrictions
      </Typography>

      <Typography
        variant='body2'
        sx={{ lineHeight: "30px" }}>
        Prohibited items. Merchants may only offer to sell items expressly
        permitted by their agreement with Uber. A merchant cannot offer
        specially regulated or illicit items, like cannabidiol (CBD) and
        tetrahydrocannabinol (THC), on their Uber Eats menu.
      </Typography>
    </Container>
  );
};

export default Restrictions;
