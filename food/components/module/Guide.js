import Link from "next/link";
import { Box, Container, Grid } from "@mui/material";

const Guide = () => {
  return (
    <Container sx={{ marginTop: "5rem" }}>
      <Grid
        sx={{ justifyContent: "center" }}
        container
        spacing={{ xs: 2, md: 3 }}
        columns={{ xs: 4, sm: 8, md: 18 }}>
        <Grid
          item
          md={4}
          xs={2}>
          <Box
            style={{
              boxShadow:
                "rgba(92, 245, 115, 0.123) 0px 4px 16px, rgba(17, 17, 26, 0.05) 0px 8px 32px",
              // padding: "15px 50px",
              width: "100%",
              height: "50px",
              borderRadius: " 10px",
              cursor: "pointer",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}>
            <Link href='/menu'>Menu</Link>
          </Box>
        </Grid>
        <Grid
          item
          md={4}
          xs={2}>
          <Box
            style={{
              boxShadow:
                "rgba(92, 245, 115, 0.123) 0px 4px 16px, rgba(17, 17, 26, 0.05) 0px 8px 32px",
              // padding: "15px 50px",
              width: "100%",
              height: "50px",
              borderRadius: " 10px",
              cursor: "pointer",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}>
            <Link href='/categories'>Categories</Link>
          </Box>
        </Grid>
        <Grid
          item
          md={4}
          xs={4}>
          <Box
            style={{
              boxShadow:
                "rgba(92, 245, 115, 0.123) 0px 4px 16px, rgba(17, 17, 26, 0.05) 0px 8px 32px",
              // padding: "15px 50px",
              width: "100%",
              height: "50px",
              borderRadius: " 10px",
              cursor: "pointer",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}>
            <Link href='/'>Discount</Link>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Guide;
