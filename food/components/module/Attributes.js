import { Box, Container, Grid, Stack, Typography } from "@mui/material";
import RocketLaunchIcon from "@mui/icons-material/RocketLaunch";
import LocalDiningIcon from "@mui/icons-material/LocalDining";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import TimelapseIcon from "@mui/icons-material/Timelapse";
import { styled } from "@mui/material/styles";

const CustomBox = styled(Box)`
  width: 150px;
  border-radius: 10px;
  box-shadow: rgba(92, 245, 115, 0.123) 0px 4px 16px,
    rgba(17, 17, 26, 0.05) 0px 8px 32px;
  text-align: center;
  padding: 25px 20px 20px;
`;

const items = [
  { id: 1, icon: <RocketLaunchIcon />, title: "Fast" },
  { id: 2, icon: <LocalDiningIcon />, title: "Best Restaurant" },
  { id: 3, icon: <CheckBoxIcon />, title: "Your choice" },
  { id: 4, icon: <TimelapseIcon />, title: "Y24 - 7" },
];

const Attributes = () => {
  return (
    <Container sx={{ marginTop: "8rem" }}>
      <Typography
        variant='h4'
        component='h4'
        color='#53c60b'>
        Why us?
      </Typography>
      <Stack
        spacing={3}
        mt={5}
        direction='row'>
        <Grid
          sx={{ justifyContent: "center" }}
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 4, sm: 8, md: 12 }}>
          {items.map((item) => (
            <Grid
              key={item.id}
              item
              md={3}>
              <CustomBox>
                <Box
                  sx={{
                    fontSize: "30px",
                    marginBottom: "15px",
                    color: "#53c60b",
                  }}>
                  {item.icon}
                </Box>
                <Typography variant='body2'>{item.title}</Typography>
              </CustomBox>
            </Grid>
          ))}
        </Grid>
      </Stack>
    </Container>
  );
};

export default Attributes;
