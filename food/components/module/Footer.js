import { Stack, Typography } from "@mui/material";

const Footer = () => {
  return (
    <Stack
      sx={{
        textAlign: "center",
        padding: "10px",
        backgroundColor: "#53c60b",
        color: "white",
      }}>
      <Typography>2SISI | Best Restaurant &copy;</Typography>
    </Stack>
  );
};

export default Footer;
