import {
  Container,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  Typography,
} from "@mui/material";

const Instruction = () => {
  return (
    <Container sx={{ mt: "4rem" }}>
      <Typography
        variant='h4'
        component='h4'
        sx={{ mb: 2 }}
        color='#53c60b'>
        How to Order?
      </Typography>

      <List sx={{ listStyleType: "disc", pl: 4 }}>
        <ListItem sx={{ display: "list-item", pl: 0 }}>
          Sign in (or create an account) and set your delivery address.
        </ListItem>
        <ListItem sx={{ display: "list-item", pl: 0 }}>
          Choose the restaurant you want to order from.
        </ListItem>
        <ListItem sx={{ display: "list-item", pl: 0 }}>
          Select your items and tap “Add to Cart”.
        </ListItem>
        <ListItem sx={{ display: "list-item", pl: 0 }}>
          To place your order, select “View cart” or “Checkout”.
        </ListItem>
        <ListItem sx={{ display: "list-item", pl: 0 }}>
          Review your order and tap “Place Order”...
        </ListItem>
        <ListItem sx={{ display: "list-item", pl: 0 }}>
          Track your order progress.
        </ListItem>
      </List>
    </Container>
  );
};

export default Instruction;
