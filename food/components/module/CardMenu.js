import {
  Badge,
  Box,
  Card,
  CardContent,
  CardMedia,
  Grid,
  Stack,
  Typography,
} from "@mui/material";
import LocalAtmIcon from "@mui/icons-material/LocalAtm";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import Link from "next/link";

const CardMenu = (props) => {
  const { id, name, price, details, discount } = props;
  return (
    <Grid
      item
      md={5}>
      <Card sx={{ pt: 2 }}>
        {discount ? (
          <Badge
            sx={{ position: "absolute" }}
            color='warning'
            badgeContent={`${discount}%`}
            showZero></Badge>
        ) : null}
        <img
          src={`/image/${id}.jpeg`}
          alt={id}
          width='250px'
          style={{ display: "table", margin: "auto", borderRadius: "5px" }}
        />

        <CardContent>
          <Stack
            direction='row'
            alignItems='center'
            justifyContent='space-between'>
            <Typography color='#53c60b'>{name}</Typography>
            <Stack
              direction='row'
              alignItems='center'>
              <LocationOnIcon sx={{ paddingRight: "5px", fontSize: "25px" }} />
              <Typography>{details[0].Cuisine}</Typography>
            </Stack>
          </Stack>

          <Stack
            direction='row'
            alignItems='center'
            sx={{ mt: 3 }}>
            <LocalAtmIcon sx={{ paddingRight: "10px", fontSize: "30px" }} />
            <Typography
              variant='body2'
              sx={{ color: discount ? "red" : "black" }}>
              {discount ? (price * (100 - discount)) / 100 : price}
            </Typography>
          </Stack>
        </CardContent>
        <Box sx={{ display: "flex", justifyContent: "center", my: 2 }}>
          <Link
            href={`/menu/${id}`}
            style={{
              backgroundColor: "#53c60b",
              color: "white",
              padding: "7px 50px ",
              borderRadius: "7px",
            }}>
            See Details
          </Link>
        </Box>
      </Card>
    </Grid>
  );
};

export default CardMenu;
